<?php
class Database{//creating the class for all the function
	private $connection;// declaring the connection for this database class
		public function readdvd(){//creating the function to display the db records of the dvd tbl 
			$sql_dvd = "SELECT * FROM `prod_dvd`";// display the prod_dvd table details 
			$res_dvd = mysqli_query($this->connection, $sql);// connecting the query with database
			return $res_dvd;// returning rslt
		}
		public function readbook(){//creating the function to display the db records of the book tbl
			$sql_book = "SELECT * FROM `prod_book`";// display the prod_book table details
			$res_book = mysqli_query($this->connection, $sql_book);// connecting the query with database
			return $res_book;// returning rslt
		}
		public function readfurt(){//creating the function to display the db records of the furniniture  tbl
			$sql_furt = "SELECT * FROM `prod_furt`";// display the prod_furt table details
			$res_furt = mysqli_query($this->connection, $sql_furt);// connecting the query with database
			return $res_furt;// returning rslt
		}
		public function delete($id){ // creating delete function for 3 tables and passing the parameter $id
			$id = $_POST['checkbox'];//taking the values of checkbox
			$sql = "DELETE FROM `prod_book` WHERE id IN (". implode(",", $id).")";//using implode to splitting up the array values
			$res_del = mysqli_query($this->connection, $sql);// connecting the query with database
			$sql = "DELETE FROM `prod_dvd` WHERE id IN (". implode(",", $id).")";
			$res_del = mysqli_query($this->connection, $sql);
			$sql = "DELETE FROM `prod_furt` WHERE id IN (". implode(",", $id).")";
			$res_del = mysqli_query($this->connection, $sql);
			if($res_del){
				return true;
			}else{
					return false;
			    }
		}
		public function connect_db(){
			$this->connection = mysqli_connect('localhost', 'root', '', 'prod');
			if(mysqli_connect_error()){
				die("Database Connection Failed" . mysqli_connect_error() . mysqli_connect_errno());
			}
		}
}
$database = new Database(); // creating object for Database Class
$database->connect_db(); // New object assigning to the connect_db()
?>